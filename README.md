Our PiHat is designed to take an inputted sound and and act as a VU meter which will show to user the "loudness" of the frequency they have selected for on the PiHat. The result will be shown on a range of LEDs which adds a visulisation element. 

#Systems for PiHat
#Subsystem 1 
This branch acts as the power module which makes sure the system has the correct power in other words this subsystem supplies the other subsystems with power 
#Subsystem 2 
This takes the input for the sound and is the filter module that analyses the input 
#Subsystem 3 
this is the visual output of the system in the form of LEDs that light up according to the specific input 
